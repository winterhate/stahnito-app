package com.winterhate.stahnito.di

interface EagerInit {
    fun eagerInit()
}
