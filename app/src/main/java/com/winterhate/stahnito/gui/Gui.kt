package com.winterhate.stahnito.gui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.winterhate.stahnito.MainActivity
import com.winterhate.stahnito.R
import com.winterhate.stahnito.api.DownloadItemStatus
import com.winterhate.stahnito.api.StahnitoApi
import com.winterhate.stahnito.di.Destructable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Gui @Inject constructor(private val mainActivity: MainActivity): Destructable {

    @Inject lateinit var downloadListAdapter: DownloadListAdapter
    @Inject lateinit var stahnitoApi: StahnitoApi

    val channel = Channel<GuiEvent>()

    override fun destroy() {
        channel.close()
    }

    fun onCreate(@Suppress("UNUSED_PARAMETER") savedInstanceState: Bundle?) = with(mainActivity) {
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            downloadListService.acceptUloztoData("Našel jsem na Ulož.to zajímavý soubor. Ke stažení zde... https://uloz.to/file/QCq1GpvIqzmA/pivni-republika-18-svarene-pivo-1080p-25fps-h264-128kbit-aac-mkv")
            Snackbar.make(view, "Data accepted", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        val recyclerView = findViewById<RecyclerView>(R.id.downloads_list)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = downloadListAdapter
        GlobalScope.launch(Dispatchers.Main) {
            channel.send(GuiEvent(GuiEvent.GuiEventType.LOAD_ALL_DOWNLOADS))
        }
    }

    fun onCreateOptionsMenu(menu: Menu): Boolean = with(mainActivity) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    private val _finishedDownloads = listOf(DownloadItemStatus.SUCCESS, DownloadItemStatus.FAIL)

    fun onOptionsItemSelected(item: MenuItem): Boolean = with(mainActivity) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when(item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                return true
            }
            R.id.clear_finished -> {
                GlobalScope.launch(Dispatchers.Main) {
                    val deleteResult = stahnitoApi.deleteDownloadsAsync(_finishedDownloads)
                    if (deleteResult.deletedCount != 0) {
                        channel.send(GuiEvent(GuiEvent.GuiEventType.LOAD_ALL_DOWNLOADS))
                    }
                }
                return true
            }
            else -> false // mainActivity.onOptionsItemSelected(item)
        }
    }

}
