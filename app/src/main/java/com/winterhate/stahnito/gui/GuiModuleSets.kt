package com.winterhate.stahnito.gui

import com.winterhate.stahnito.di.Destructable
import com.winterhate.stahnito.di.EagerInit
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet

@Module
abstract class GuiModuleSets {

    @Binds @IntoSet
    abstract fun guiEventListenerAsEagerInit(guiEventListener: GuiEventListener): EagerInit

    @Binds @IntoSet
    abstract fun guiAsDestructable(gui: Gui): Destructable

}
