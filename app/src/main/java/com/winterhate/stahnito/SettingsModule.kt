package com.winterhate.stahnito

import android.content.SharedPreferences
import com.winterhate.stahnito.api.ServerConnectionProperties
import dagger.Module
import dagger.Provides
import java.net.URL
import javax.inject.Inject
import javax.inject.Singleton

@Module
class SettingsModule @Inject constructor(private val preferences: SharedPreferences) {

    @Singleton @Provides fun getServerConnectionProperties(): ServerConnectionProperties = ServerConnectionProperties(URL(preferences.getString("stahnito_url", null)))

}
