package com.winterhate.stahnito.gui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.log4k.e
import com.winterhate.stahnito.R
import com.winterhate.stahnito.api.DownloadItemReference
import com.winterhate.stahnito.api.StahnitoApi
import com.winterhate.stahnito.gui.GuiModule.Companion.DOWNLOAD_LIST_INFLATER
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class DownloadListAdapter @Inject constructor(
    private val stahnitoApi: StahnitoApi,
    @Named(DOWNLOAD_LIST_INFLATER) private val inflater: LayoutInflater
): RecyclerView.Adapter<DownloadListAdapter.DownloadListViewHolder>() {

    private var downloadList: List<DownloadItemReference> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DownloadListViewHolder =
        DownloadListViewHolder(inflater.inflate(R.layout.download_list_row, parent, false))

    override fun getItemCount(): Int = downloadList.size

    override fun onBindViewHolder(holder: DownloadListViewHolder, position: Int) =
        holder.updateWith(downloadList[position], position)

    suspend fun loadAllDownloads() {
        try {
            downloadList = stahnitoApi.findAllDownloadsAsync()
            notifyDataSetChanged()
        } catch (ex: Exception) {
            e(ex.message.orEmpty())
        }
    }

    inner class DownloadListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val filenameView: TextView = itemView.findViewById(R.id.filename)
        private val updatedView: TextView = itemView.findViewById(R.id.updated)
        private val positionView: TextView = itemView.findViewById(R.id.position)

        private fun positionLabel(position: Int): String = (position + 1).toString()

        fun updateWith(itemReference: DownloadItemReference, position: Int) {
            positionView.text = positionLabel(position)
            fetchDownloadItem(itemReference)
        }

        private fun format(): DateFormat =
            DateFormat.getDateTimeInstance(
                DateFormat.DEFAULT,
                DateFormat.DEFAULT,
                Locale.getDefault()
            )

        private fun fetchDownloadItem(itemReference: DownloadItemReference) {
            GlobalScope.launch(Dispatchers.Main) {
                val download = stahnitoApi.findDownloadByUuidAsync(itemReference.uuid)
                filenameView.text = download.filename
                updatedView.text = format().format(Date.from(download.updated))
            }
        }

    }

}