package com.winterhate.stahnito.service

import com.log4k.d
import com.log4k.i
import com.log4k.w
import com.winterhate.stahnito.api.SaveUniqueDownloadData
import com.winterhate.stahnito.api.StahnitoApi
import com.winterhate.stahnito.gui.Gui
import com.winterhate.stahnito.gui.GuiEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.URL
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DownloadListService @Inject constructor(private val stahnito: StahnitoApi) {

    @Inject lateinit var gui: Gui

    fun acceptUloztoData(uloztoData: String) {
        i("Accepting data: ${uloztoData}")
        prepareUloztoData(uloztoData)?.let { saveUloztoData(it) }
    }

    private fun saveUloztoData(uloztoData: String) {
        i("Saving data: ${uloztoData}")
        GlobalScope.launch(Dispatchers.Main) {
            val downloadUuid = UUID.randomUUID()
            val uniqueDownload = stahnito.saveUniqueDownloadAsync(downloadUuid, SaveUniqueDownloadData(URL(uloztoData)))
            if (uniqueDownload.isSuccessful) {
                i("UloztoData: $uloztoData, status: ${uniqueDownload.body()?.status}")
                gui.channel.send(GuiEvent(GuiEvent.GuiEventType.LOAD_ALL_DOWNLOADS))
            } else {
                w("Unsuccessful call: ${uniqueDownload.code()}")
            }
        }
    }

    private fun prepareUloztoData(intentData: String?): String? {
        if (intentData == null || !intentData.contains("https://uloz.to")) {
            d("Data not useful")
            return null
        }
        return intentData.replace(Regex("""^.+https://"""), "https://")
    }

}