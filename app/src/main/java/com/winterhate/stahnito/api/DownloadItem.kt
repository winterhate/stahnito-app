package com.winterhate.stahnito.api

import com.fasterxml.jackson.annotation.JsonCreator
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.net.URL
import java.time.Instant
import java.util.*

enum class DownloadItemStatus {
    NEW, IN_PROGRESS, SUCCESS, FAIL;

    companion object {
        private val deserializationMap = mapOf("New" to NEW, "In progress" to IN_PROGRESS, "Success" to SUCCESS,
            "Fail" to FAIL)

        private val serializationMap = deserializationMap.entries.associate { (k, v) -> v to k }

        @JvmStatic @JsonCreator
        fun deserialize(serialization: String): DownloadItemStatus = deserializationMap.getValue(serialization)

        @JvmStatic
        fun serialize(downloadItemStatus: DownloadItemStatus): String? = serializationMap[downloadItemStatus]
    }
}

class DownloadItemStatusConverterFactory: Converter.Factory() {

    class DownloadItemStatusConverter: Converter<DownloadItemStatus, String> {
        override fun convert(value: DownloadItemStatus): String? = DownloadItemStatus.serialize(value)
    }

    private val converter by lazy { DownloadItemStatusConverter() }

    override fun stringConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<DownloadItemStatus, String>?
        = if (type == DownloadItemStatus::class.java) converter else null
}

data class Reason(val stdout: String, val stderr: String)

data class DownloadItem(val uuid: UUID, val url: URL, val status: DownloadItemStatus,
                        val filename: String, val reason: Reason,
                        val created: Instant, val updated: Instant)

data class DownloadItemReference(val uuid: UUID)

data class SaveUniqueDownloadData(val url: URL)

enum class ServerStatusEnum { CREATED, DUPLICATE, DELETED }

data class ServerStatus(val status: ServerStatusEnum)

data class DeleteDownloadsStatus(val status: ServerStatusEnum, val deletedCount: Int)
