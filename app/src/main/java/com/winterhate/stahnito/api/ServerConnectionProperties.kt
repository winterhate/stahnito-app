package com.winterhate.stahnito.api

import java.net.URL

data class ServerConnectionProperties(val url: URL)