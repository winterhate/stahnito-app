package com.winterhate.stahnito.gui

import android.view.LayoutInflater
import com.winterhate.stahnito.MainActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.channels.Channel
import javax.inject.Named
import javax.inject.Singleton

@Module
class GuiModule(private val mainActivity: MainActivity) {

    companion object {
        const val DOWNLOAD_LIST_INFLATER = "downloadListInflater"
    }

    @Provides @Singleton
    @Named(DOWNLOAD_LIST_INFLATER) fun inflater(): LayoutInflater = LayoutInflater.from(mainActivity)

    @Provides fun mainActivity(): MainActivity = mainActivity

    @Provides @Singleton fun provideGuiEventsChannel() = Channel<GuiEvent>()

}
