package com.winterhate.stahnito.api

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.winterhate.stahnito.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton @Provides fun okHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }

        return builder.build()
    }

    @Singleton @Provides fun stahnitoApi(serverConnectionProperties: ServerConnectionProperties, okHttpClient: OkHttpClient): StahnitoApi {
        val stahnitoObjectMapper: ObjectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())
        val urlExternalForm = serverConnectionProperties.url.toExternalForm()
        Log.d("StahnitoApi", "Creating retrofit client for $urlExternalForm")
        val retrofit = Retrofit.Builder()
            .baseUrl(urlExternalForm)
            .addConverterFactory(JacksonConverterFactory.create(stahnitoObjectMapper))
            .addConverterFactory(DownloadItemStatusConverterFactory())
            .client(okHttpClient)
            .build()
        return retrofit.create(StahnitoApi::class.java)
    }

}
