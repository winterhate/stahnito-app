package com.winterhate.stahnito

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.log4k.Level
import com.log4k.Log4k
import com.log4k.android.AndroidAppender
import com.winterhate.stahnito.api.NetworkModule
import com.winterhate.stahnito.di.Destructable
import com.winterhate.stahnito.di.EagerInit
import com.winterhate.stahnito.gui.Gui
import com.winterhate.stahnito.gui.GuiModule
import com.winterhate.stahnito.service.DownloadListService
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject lateinit var gui: Gui
    @Inject lateinit var downloadListService: DownloadListService

    private lateinit var component: StahnitoComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        if (BuildConfig.DEBUG) { Log4k.add(Level.Verbose, ".*", AndroidAppender()) }
        super.onCreate(savedInstanceState)

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        component = DaggerStahnitoComponent.builder()
            .guiModule(GuiModule(this))
            .settingsModule(SettingsModule(sharedPref))
            .networkModule(NetworkModule())
            .build()

        component.inject(this)
        component.getEagerInits().forEach(EagerInit::eagerInit)

        gui.onCreate(savedInstanceState)

        processIntent()
    }

    override fun onDestroy() {
        super.onDestroy()
        component.getDestructables().forEach(Destructable::destroy)
    }

    private fun processIntent() {
        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    downloadListService.acceptUloztoData(intent.getStringExtra(Intent.EXTRA_TEXT))
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean = gui.onCreateOptionsMenu(menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean = gui.onOptionsItemSelected(item)

}
