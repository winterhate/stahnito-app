package com.winterhate.stahnito.gui

import com.log4k.d
import com.winterhate.stahnito.di.EagerInit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class GuiEventListener @Inject constructor(val gui: Gui): EagerInit {

    @Inject lateinit var downloadListAdapter: DownloadListAdapter

    override fun eagerInit() {
        GlobalScope.launch(Dispatchers.Main) {
            for(event in gui.channel) {
                d("Processing ${event}")
                when {
                    event.guiEventType == GuiEvent.GuiEventType.LOAD_ALL_DOWNLOADS -> {
                        downloadListAdapter.loadAllDownloads()
                    }
                    else -> {
                        discard(event)
                    }
                }
            }
        }
    }

    private fun discard(event: GuiEvent) {
        d("Discarding ${event}")
    }

}