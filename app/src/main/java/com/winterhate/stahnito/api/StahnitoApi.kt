package com.winterhate.stahnito.api

import retrofit2.Response
import retrofit2.http.*
import java.util.*

interface StahnitoApi {

    @GET("/api/downloads")
    suspend fun findAllDownloadsAsync(): List<DownloadItemReference>

    @POST("/api/downloads/{uuid}")
    suspend fun saveUniqueDownloadAsync(@Path("uuid") uuid: UUID, @Body url: SaveUniqueDownloadData): Response<ServerStatus>

    @GET("/api/downloads/{uuid}")
    suspend fun findDownloadByUuidAsync(@Path("uuid") uuid: UUID): DownloadItem

    @DELETE("/api/downloads/{uuid}")
    suspend fun deleteDownloadAsync(@Path("uuid") uuid: UUID): DeleteDownloadsStatus

    @DELETE("/api/downloads/")
    suspend fun deleteDownloadsAsync(@Query("status[]") statuses: List<@JvmSuppressWildcards DownloadItemStatus>): DeleteDownloadsStatus

}
