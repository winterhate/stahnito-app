package com.winterhate.stahnito.di

interface Destructable {
    fun destroy()
}