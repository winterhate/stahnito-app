package com.winterhate.stahnito.gui

data class GuiEvent(val guiEventType: GuiEventType) {
    enum class GuiEventType {
        LOAD_ALL_DOWNLOADS
    }
}

