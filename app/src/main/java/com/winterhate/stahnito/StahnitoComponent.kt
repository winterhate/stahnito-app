package com.winterhate.stahnito

import com.winterhate.stahnito.api.NetworkModule
import com.winterhate.stahnito.di.Destructable
import com.winterhate.stahnito.di.EagerInit
import com.winterhate.stahnito.gui.GuiModule
import com.winterhate.stahnito.gui.GuiModuleSets
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ GuiModule::class, GuiModuleSets::class, SettingsModule::class, NetworkModule::class ])
interface StahnitoComponent {
    fun inject(mainActivity: MainActivity)
    fun getEagerInits(): Set<EagerInit>
    fun getDestructables(): Set<Destructable>
}
